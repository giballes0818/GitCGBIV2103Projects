package com.cy.controller;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

//@Controller
//@ResponseBody
@RestController
@RequestMapping("/provider/")
public class RestProviderController {


    //更新操作
    @PutMapping
    public Map<String,Object> doUpdate(
            @RequestBody  Map<String,Object> map){
        //....后续要将请求数据写入到数据库
        System.out.println("provider.map="+map);
        //定义响应结果
        Map<String,Object> responseMap=new HashMap<>();
        responseMap.put("status",200);
        responseMap.put("message","update ok");
        return responseMap;
    }
    //添加操作
    @PostMapping
    public Map<String,Object> doCreate(
            @RequestBody  Map<String,Object> map){
        //....后续要将请求数据写入到数据库
        System.out.println("provider.map="+map);
        //定义响应结果
        Map<String,Object> responseMap=new HashMap<>();
        responseMap.put("status",200);
        responseMap.put("message","insert ok");
        return responseMap;
    }
    @DeleteMapping("{id}")
    public String doDeleteById(@PathVariable Long id){
        System.out.println("provider.id="+id);
        return id +" is deleted";
    }
}
