package com.cy.service;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import org.springframework.stereotype.Service;

@Service
public class ConsumerService {
    @SentinelResource
    public String doConsumerService(){
        return "hello world";
    }
}
