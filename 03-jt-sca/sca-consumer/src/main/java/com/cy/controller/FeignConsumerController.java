package com.cy.controller;

import com.cy.service.RemoteProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/consumer/")
public class FeignConsumerController {
    @Value("${spring.application.name}")
    private String serverName;
    //Feign API
    @Autowired
    private RemoteProviderService remoteProviderService;

    //http://ip:port/consumer/hello
    @GetMapping("/echo/{msg}")
    public String doEcho(@PathVariable String msg){
        //执行远程调用
        return remoteProviderService.echoMessage(msg);
    }

}
