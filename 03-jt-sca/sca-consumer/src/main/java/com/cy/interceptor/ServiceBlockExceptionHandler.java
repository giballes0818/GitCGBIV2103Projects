package com.cy.interceptor;

import com.alibaba.csp.sentinel.adapter.spring.webmvc.callback.BlockExceptionHandler;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.PrintWriter;

@Component
public class ServiceBlockExceptionHandler
         implements BlockExceptionHandler {
    @Override
    public void handle(HttpServletRequest request,
                       HttpServletResponse response,
                       BlockException e) throws Exception {
        //设置响应数据的编码
        response.setCharacterEncoding("utf-8");
        //告诉浏览器要响应的内容格式,以及浏览器应该以什么编码进行显示
        response.setContentType("application/json;charset=utf-8");
        PrintWriter out=response.getWriter();
        //向客户端输出一个json格式字符串
        out.println("{\"code\":\"601\",\"msg\":\"访问太频繁,稍等片刻再访问\"}");
        out.flush();
    }
}
