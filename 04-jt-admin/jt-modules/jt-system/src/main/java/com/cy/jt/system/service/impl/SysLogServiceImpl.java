package com.cy.jt.system.service.impl;

import com.cy.jt.system.dao.SysLogDao;
import com.cy.jt.system.domain.SysLog;
import com.cy.jt.system.service.SysLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class SysLogServiceImpl implements SysLogService {
    @Autowired
    private SysLogDao sysLogDao;
    //  public SysLogServiceImpl(SysLogDao sysLogDao){
//        this.sysLogDao=sysLogDao;
//  }
    @Override
    public List<SysLog> selectLogs(SysLog sysLog) {
        return sysLogDao.selectLogs(sysLog);
    }

    @Override
    public SysLog selectById(Long id) {
        return sysLogDao.selectById(id);
    }

    @Async //由此注解描述的方法为异步切入点方法
    @Override
    public void insertLog(SysLog sysLog) {
       String tName=Thread.currentThread().getName();
       System.out.println("SysLogService.thread.name="+tName);
       //try{ Thread.sleep(3000);}
       //catch (Exception e){}
       sysLogDao.insertLog(sysLog);
    }

    @Override
    public int deleteById(Long... id) {
        return sysLogDao.deleteById(id);
    }

}
