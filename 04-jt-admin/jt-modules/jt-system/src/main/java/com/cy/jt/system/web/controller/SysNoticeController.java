package com.cy.jt.system.web.controller;
import com.cy.jt.common.annotaion.RequiredLog;
import com.cy.jt.common.domain.JsonResult;
import com.cy.jt.system.domain.SysNotice;
import com.cy.jt.system.service.SysNoticeService;
import com.cy.jt.system.web.util.PageUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/notice/")
public class SysNoticeController {
     @Autowired
     private SysNoticeService sysNoticeService;

     @PostMapping
     public JsonResult doInsertNotice(@RequestBody SysNotice notice){
         sysNoticeService.insertNotice(notice);
         return new JsonResult("insert ok");
     }

    /**
     * @CacheEvict 表示清除缓存,value的值为缓存名称,
     * allEntries=true表示清除所有,
     * beforeInvocation是在方法执行之前还是之后执行
     * @param notice
     * @return
     */
     //@CacheEvict(value="notices",allEntries = true,beforeInvocation = false)
     @PutMapping
     public JsonResult doUpdateNotice(@RequestBody SysNotice notice){
        sysNoticeService.updateNotice(notice);
        return new JsonResult("update ok");
     }

     @DeleteMapping("{id}")
     public JsonResult doDeleteById(@PathVariable Long ...id){
         sysNoticeService.deleteById(id);
         return new JsonResult("delete ok");
     }

    /*
     * @Cacheable 描述方法时,系统底层在执行时会将查询结果存储到缓存(本地缓存,redis),
     * 下次查询时会直接从缓存查询,来提高查询性能.
     * @param id
     * @return
     */
     @RequiredLog(operation="基于id查询通告信息")
     @Cacheable(value = "notices")
     @GetMapping("{id}")
     public JsonResult doSelectById(@PathVariable Long id){
        return new JsonResult(sysNoticeService.selectById(id));
     }
    /**
     * @Cacheable 这个注解描述的方法,在执行时,系统底层会先去查询缓存
     * @param sysNotice
     * @return
     */
     //@RequiredLog(operation="查询通告信息")
     //@Cacheable(value = "notices")//这里notices表示缓存的名称
     @GetMapping
     public JsonResult doSelectNotices(SysNotice sysNotice){
         //上面分页的简化写法就是如下方式
         return new JsonResult(PageUtils.startPage()//启动分页拦截器
                    .doSelectPageInfo(()->{//执行查询业务
                      sysNoticeService.selectNotices(sysNotice); }));
     }
}

