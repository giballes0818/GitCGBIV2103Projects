package com.cy.jt.demo.service;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class MemoryService {
    //将来这个方法要在写库写数据,同时还要向内存写数据
    public void insert(Map<String,Object> map){//
        //....
    }
    //后续此方法首先读本地缓存数据,本地没有则读分布式缓存,分布式也没有则读读库中数据
    //本地缓存(JVM内部的缓存)
    @Cacheable(value="myCache") //由此注解描述的方法为缓存切入点方法
    public List<Map<String,Object>> list(){
        System.out.println("Get Data from Database");
        //假设如下数据来自于数据库
        Map<String,Object> m1=new HashMap<>();
        m1.put("id",100);
        m1.put("title","title-A");
        Map<String,Object> m2=new HashMap<>();
        m2.put("id",101);
        m2.put("title","title-B");
        List<Map<String,Object>> list=new ArrayList<>();
        list.add(m1);
        list.add(m2);
        return list;
    }
}