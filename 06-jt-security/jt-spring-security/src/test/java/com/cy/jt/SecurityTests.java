package com.cy.jt;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.DigestUtils;

@SpringBootTest
public class SecurityTests {
    @Test
    void testMd5(){
        String password="123456";
        String newPwd=DigestUtils.md5DigestAsHex(password.getBytes());
        System.out.println(newPwd);
        //e10adc3949ba59abbe56e057f20f883e
        //e10adc3949ba59abbe56e057f20f883e
    }
    @Test
    void testEncodePassword(){
        //1.定义密码
        String password="123456";
        //2.构建加密对象
        BCryptPasswordEncoder encoder=new BCryptPasswordEncoder();
        //3.对密码进行加密
        String encodePwd = encoder.encode(password);
        System.out.println(encodePwd);
        //$2a$10$1DDSpZEnzOjS5lStF0kOJuV7Y8mQIMzKUBH8QUGGDFuWV35ZkEYuW
        //$2a$10$iAAcQ6ZEDqVtbD8HCdnaKekmx5IRUMRhUVgnLla47Xgoonl2d3oMW
        //$2a$10$Tq34FGkH99.vGpd/Ih3YQOB5cz.YLxfG2UQZjWpZOswGgoVK0V19q
    }
    @Test
    void testMatchPassword(){
        //1.定义密码
        String password="123456";
        //2.构建加密对象
        BCryptPasswordEncoder encoder=new BCryptPasswordEncoder();
        boolean flag = encoder.matches(password,
                        "$2a$10$iAAcQ6ZEDqVtbD8HCdnaKekmx5IRUMRhUVgnLla47Xgoonl2d3oMW");
        System.out.println("flag="+flag);
    }
}
