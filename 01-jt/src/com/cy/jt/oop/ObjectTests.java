package com.cy.jt.oop;
import java.util.HashMap;
import java.util.Map;
class Cache{
    public static Map<String,Object> cache=new HashMap<>();
    public static Cache instance=new Cache();
    public Cache(){
        cache.put("A",100);
    }
    public static void doPrint(){
        System.out.println(cache);
    }
}
public class ObjectTests {
    public static void main(String[] args) {
       Cache.doPrint();
    }
}
