package com.cy.jt.net;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Tomcat {//JVM

    public static void main(String[] args) throws IOException {
        ServerSocket server=new ServerSocket(9999);
        System.out.println("server is starting");
        boolean isFlag=true;
        while(isFlag){
           Socket socket=server.accept();
            System.out.println("client="+socket);
            ObjectInputStream in=
                    new ObjectInputStream(socket.getInputStream());
            String content=in.readUTF();
            System.out.println("client say:"+content);
        }
        server.close();
    }
}
