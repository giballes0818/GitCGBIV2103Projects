package com.cy.jt.net;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class Nginx {
    public static void main(String[] args) throws IOException {
        ServerSocket server=new ServerSocket(10001);
        System.out.println("nginx start ok,do listener 10001");
        while(true){
            System.out.println("waiting for client");
            Socket socket=server.accept();//阻塞式方法(没有客户端连接时,线程会在这里阻塞)
            System.out.println("client="+socket);
            ObjectInputStream in=
            new ObjectInputStream(socket.getInputStream());
            String content=in.readUTF();
            //System.out.println("client say:"+content);
            new Thread(){
                @Override
                public void run() {
                    try {
                        Socket tomcatClient = new Socket("127.0.0.1", 9999);
                        //获取输出流对象
                        ObjectOutputStream out =
                                new ObjectOutputStream(tomcatClient.getOutputStream());
                        //向对应的服务端写数据
                        out.writeUTF(content);
                        out.flush();
                        ;
                        //关闭客户端对象
                        tomcatClient.close();
                    }catch (Exception e){}
                }
            }.start();

        }
    }
}
