package com.cy.jt.net;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Client {
    public static void main(String[] args) throws IOException {
        //客户端对象
        Socket socket=new Socket("127.0.0.1",10001);
        //获取输出流对象
        ObjectOutputStream out=
                new ObjectOutputStream(socket.getOutputStream());
        //向对应的服务端写数据
        out.writeUTF("hello tomcat");
        out.flush();;
        //关闭客户端对象
        socket.close();

    }
}
