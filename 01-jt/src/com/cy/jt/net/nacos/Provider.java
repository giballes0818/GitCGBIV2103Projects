package com.cy.jt.net.nacos;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class Provider {
    public static void main(String[] args) throws IOException {
        //启动客户端
        Socket socket=new Socket("127.0.0.1",8848);
        ObjectOutputStream out=
                new ObjectOutputStream(socket.getOutputStream());
        //向nacos发送消息
        out.writeUTF("sca-provider");//服务名
        out.flush();
        socket.close();
    }
}
