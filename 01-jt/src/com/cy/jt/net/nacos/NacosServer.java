package com.cy.jt.net.nacos;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class NacosServer {
    /**
     * 外层map中的key为服务名,值为一个实例集合
     */
    private static Map<String, List<Socket>> registerCenter=
            new ConcurrentHashMap<>();
    public static void main(String[] args) throws IOException {
        ServerSocket server=new ServerSocket(8848);
        System.out.println("服务已启动");
        while(true){
          Socket socket = server.accept();
          ObjectInputStream in=
           new ObjectInputStream(socket.getInputStream());
          String serverName=in.readUTF();
          System.out.println("serverName="+serverName);
          int port=socket.getPort();
          InetAddress inetAddress=socket.getInetAddress();
          String hostAddress=inetAddress.getHostAddress();
          String client=hostAddress+"/"+port;
          System.out.println("client="+client);
          if(registerCenter.containsKey(serverName)){
              registerCenter.get(serverName).add(socket);
          }else{
              List<Socket> list=new ArrayList<>();
              list.add(socket);
              registerCenter.put(serverName,list);
          }
          System.out.println(registerCenter);
        }
    }
}
